import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClubsTableComponent } from './component/clubs-table.component';
import { TableModule } from 'primeng-lts/table';
import { SharedPipesModule } from '../../pipes/shared-pipes.module';
import { CardModule } from 'primeng-lts/card';

@NgModule({
  declarations: [ClubsTableComponent],
  exports: [ClubsTableComponent],
  imports: [CommonModule, TableModule, SharedPipesModule, CardModule],
})
export class ClubsTableModule {}
