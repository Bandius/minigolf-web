import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayersTableComponent } from './component/players-table.component';
import { TableModule } from 'primeng-lts/table';
import {SharedPipesModule} from "../../pipes/shared-pipes.module";

@NgModule({
  declarations: [PlayersTableComponent],
  exports: [PlayersTableComponent],
  imports: [CommonModule, TableModule, SharedPipesModule],
})
export class PlayersTableModule {}
