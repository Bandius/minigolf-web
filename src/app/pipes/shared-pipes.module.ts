import { NgModule } from '@angular/core';
import { ClubByIdPipe } from './club-by-id.pipe';
import { PlayerNamePipe } from './player-name.pipe';
import { ScoreSumPipe } from './score-sum.pipe';
import { CoursesLanesPipe } from './courses-lanes.pipe';
import { ClubByIdFromServicePipe } from './club-by-id-from-service.pipe';
import { RoundsSumPipe } from './rounds-sum.pipe';
import { PenaltyTablePipe } from './penalty-table.pipe';
import { RoundColorPipe } from './round-color.pipe';
import { ScoreColorPipe } from './score-color.pipe';
import { TeamRoundColorPipe } from './team-round-color.pipe';
import { MenuFilterPipe } from './menu-filter.pipe';
import { ScoreByCategoryPipe } from './score-by-category.pipe';
import { PlayerByIdPipe } from './player-by-id.pipe';
import { NasadenieByScorePipe } from './nasadenie-by-score.pipe';
import { PlayerFindPipe } from './player-find.pipe';
import { ScoreOrderPipe } from './score-order.pipe';
import { EventTagPipe } from './event-tag.pipe';
import { IncludedFilterPipe } from './included-filter.pipe';

@NgModule({
  declarations: [
    ClubByIdPipe,
    PlayerNamePipe,
    ScoreSumPipe,
    CoursesLanesPipe,
    ClubByIdFromServicePipe,
    RoundsSumPipe,
    PenaltyTablePipe,
    RoundColorPipe,
    ScoreColorPipe,
    TeamRoundColorPipe,
    MenuFilterPipe,
    ScoreByCategoryPipe,
    PlayerByIdPipe,
    NasadenieByScorePipe,
    PlayerFindPipe,
    ScoreOrderPipe,
    EventTagPipe,
    IncludedFilterPipe,
  ],
  exports: [
    ClubByIdPipe,
    PlayerNamePipe,
    ScoreSumPipe,
    CoursesLanesPipe,
    ClubByIdFromServicePipe,
    RoundsSumPipe,
    PenaltyTablePipe,
    ScoreColorPipe,
    RoundColorPipe,
    TeamRoundColorPipe,
    MenuFilterPipe,
    ScoreByCategoryPipe,
    PlayerByIdPipe,
    NasadenieByScorePipe,
    PlayerFindPipe,
    ScoreOrderPipe,
    EventTagPipe,
    IncludedFilterPipe,
  ],
  providers: [ClubByIdPipe, ScoreSumPipe, RoundsSumPipe],
})
export class SharedPipesModule {}
