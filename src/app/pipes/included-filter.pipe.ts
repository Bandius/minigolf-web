import { Pipe, PipeTransform } from "@angular/core";
import { Round } from "../models/round";

@Pipe({
  name: "includedFilter"
})
export class IncludedFilterPipe implements PipeTransform {

  transform(rounds: Round[]): Round[] {
    return rounds?.filter(r => r.includeInTeam || r.includeInTeam === undefined);
  }
}
