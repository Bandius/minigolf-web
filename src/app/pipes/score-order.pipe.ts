import { Pipe, PipeTransform } from '@angular/core';
import { Score } from '../models/score';
import { Round } from '../models/round';
import { ScoreRound } from '../models/score-round';

@Pipe({
  name: 'scoreOrder',
})
export class ScoreOrderPipe implements PipeTransform {
  transform(scores: Score[], rounds: Round[]): Score[] {
    return scores
      ?.map((s) => {
        s.diffs = ScoreOrderPipe.diffs(s.rounds, rounds);
        return s;
      })
      .sort((a, b) => {
        const diffsLength = Math.min(a.diffs.length, b.diffs.length);
        for (let i = 0; i < diffsLength; ++i) {
          if (a.diffs[i] !== b.diffs[i]) return a.diffs[i] - b.diffs[i];
        }
        return -1;
      })
      .sort((a, b) => {
        if (!isFinite(a.average) && !isFinite(b.average)) return 0;
        if (!isFinite(a.average)) return 1;
        if (!isFinite(b.average)) return -1;
        return a.average - b.average;
      });
  }

  static diffs(scoreRounds: ScoreRound[], rounds: Round[]): number[] {
    if (rounds.length < 2) return [];

    const roundSums: { [courseId: number]: number[] } = {};
    rounds.forEach((r, i) => {
      if (!roundSums[r.courseId]) roundSums[r.courseId] = [];
      if (scoreRounds[i]?.sum > 0) {
        roundSums[r.courseId].push(scoreRounds[i].sum);
      }
    });

    const diffsCount = Math.floor(
      Math.min(...Object.values(roundSums).map((v) => v.length)) / 2
    );
    const diffs: number[][] = [];

    Object.keys(roundSums).forEach((k, idx) => {
      roundSums[k] = roundSums[k].sort((a, b) => a - b);
      const roundSumsLength = roundSums[k].length;
      diffs.push([]);
      for (let i = 0; i < diffsCount; ++i) {
        diffs[idx].push(
          roundSums[k][roundSumsLength - 1 - i] - roundSums[k][i]
        );
      }
    });

    const diffsLength = diffs.length;

    if (diffsLength === 1) return diffs[0];

    const summed: number[] = [];
    for (let i = 0; i < diffsCount; ++i) {
      let sum = 0;
      for (let n = 0; n < diffsLength; ++n) {
        sum += diffs[n][i];
      }
      summed.push(sum);
    }
    return summed;
  }
}
