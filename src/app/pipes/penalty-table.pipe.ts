import { Pipe, PipeTransform } from '@angular/core';
import { PenaltyType } from '../models/penalty-type';
import { Score } from '../models/score';

@Pipe({
  name: 'penaltyTable',
})
export class PenaltyTablePipe implements PipeTransform {
  transform(score: Score): string {
    if (score.penalty.some((v) => v.type === PenaltyType.D)) {
      return PenaltyType.D;
    } else if (score.penalty.some((v) => v.type === PenaltyType.B)) {
      return score.penaltySum.toString();
    } else if (score.penalty.some((v) => v.type === PenaltyType.N)) {
      return PenaltyType.N;
    } else {
      return null;
    }
  }
}
