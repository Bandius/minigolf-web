import { Pipe, PipeTransform } from '@angular/core';
import { ScoreSumPipe } from './score-sum.pipe';
import {ScoreRound} from "../models/score-round";

@Pipe({
  name: 'roundsSum',
})
export class RoundsSumPipe implements PipeTransform {
  constructor(private scoreSumPipe: ScoreSumPipe) {}

  transform(rounds: ScoreRound[]): number {
    return rounds?.reduce(
      (sum, round) => sum + this.scoreSumPipe.transform(round),
      0
    );
  }
}
