import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { Login } from '../model/login';
import { take, takeUntil } from 'rxjs/operators';
import { MenuAddonsService } from '../../../services/menu-addons.service';
import { MenuButton } from '../../../models/menu-button';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  destroy$: Subject<void> = new Subject<void>();
  formGroup: FormGroup = this.fb.group({
    login: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    public loginService: LoginService,
    private menuAddonsService: MenuAddonsService
  ) {}

  ngOnInit(): void {
    this.loginService.loggedIn$.pipe(takeUntil(this.destroy$)).subscribe(
      (login) => {
        if (login) {
          setTimeout(() => {
            this.menuAddonsService.setButtons([
              new MenuButton(
                'login-btn',
                this.onLogout.bind(this),
                'Odhlásiť',
                'pi pi-sign-out',
                ''
              ),
            ]);
          }, 0)
        } else {
          this.menuAddonsService.setButtons([]);
        }
      },
      null,
      () => {
        this.menuAddonsService.setButtons([]);
      }
    );
  }

  onSubmit(): void {
    this.loginService
      .getLogin(
        new Login(this.formGroup.value.login, this.formGroup.value.password)
      )
      .pipe(take(1))
      .subscribe(() => {});
  }

  onLogout(): void {
    this.loginService.logout();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
