export class UserCreate {
  login: string;
  password: string;
  eventId: number;

  constructor(login: string, password: string, eventId: number) {
    this.login = login;
    this.password = password;
    this.eventId = eventId;
  }
}
