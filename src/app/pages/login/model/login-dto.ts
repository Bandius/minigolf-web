import { Role } from './role';

export class LoginDto {
  id: number;
  created: string;
  login: string;
  role: Role;
  token: string;
  eventId: number;

  constructor(token: string) {
    this.token = token;
  }
}
