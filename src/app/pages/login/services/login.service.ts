import { Injectable } from '@angular/core';
import { Login } from '../model/login';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoginDto } from '../model/login-dto';
import { LoginHttpService } from './http/login-http.service';
import { map, take, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UserCreate } from '../model/user-create';
import { Empty } from '../../../models/empty';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private login: BehaviorSubject<LoginDto> = new BehaviorSubject<LoginDto>(
    null
  );

  constructor(
    private loginHttpService: LoginHttpService,
    private router: Router
  ) {}

  get loggedIn$(): Observable<boolean> {
    return this.login$.pipe(map((v) => !!v?.id));
  }

  get login$(): Observable<LoginDto> {
    return this.login.asObservable();
  }

  get loginValue(): LoginDto {
    return this.login.value;
  }

  get token(): string {
    return this.login.value?.token;
  }

  getLogin(login: Login): Observable<LoginDto> {
    return this.loginHttpService.login(login).pipe(
      tap((login) => {
        localStorage.setItem('token', JSON.stringify(login.token));
        this.login.next(login);
      })
    );
  }

  createUser(userCreate: UserCreate): Observable<Empty> {
    return this.loginHttpService.createUser(userCreate);
  }

  logout(): void {
    localStorage.removeItem('token');
    this.login.next(null);
  }

  logoutAndRedirectToLogin(): void {
    this.logout();
    this.router.navigate(['/login']);
  }

  tryLogin(): void {
    const token = JSON.parse(localStorage.getItem('token'));
    if (token) {
      this.login.next(new LoginDto(token));
      this.loginHttpService.getLogin().subscribe((login) => {
        localStorage.setItem('token', JSON.stringify(login.token));
        this.login.next(login);
      });
    }
  }

  canRead(): Observable<boolean> {
    return this.loggedIn$.pipe(
      map((login) => login),
      take(1)
    );
  }
}
