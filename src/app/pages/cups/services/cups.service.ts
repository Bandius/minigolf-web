import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { Empty } from '../../../models/empty';
import { CupsHttpService } from './http/cups-http.service';
import { CupDto } from '../../../models/cup-dto';
import { CupCreateUpdate } from '../../../models/cup-create-update';

@Injectable({
  providedIn: 'root',
})
export class CupsService {
  private cups: BehaviorSubject<CupDto[]> = new BehaviorSubject<CupDto[]>(
    undefined
  );
  private selectedCup: BehaviorSubject<CupDto> = new BehaviorSubject<CupDto>(
    null
  );
  private loading: boolean;

  constructor(private cupsHttpService: CupsHttpService) {
    this.reset();
  }

  get cups$(): Observable<CupDto[]> {
    if (this.cups.value || this.loading) {
      return this.cups.asObservable();
    } else {
      this.reset();
      return this.cups.asObservable();
    }
  }

  get selectedCup$(): Observable<CupDto> {
    return this.selectedCup.asObservable();
  }

  public reset(): void {
    this.loading = true;
    this.getAll()
      .pipe(take(1))
      .subscribe(() => {
        this.loading = false;
      });
  }

  public getAll(): Observable<CupDto[]> {
    return this.cupsHttpService.getAll().pipe(
      tap((cupDtos) => {
        this.cups.next(cupDtos);
      })
    );
  }

  public create(cupCreateUpdate: CupCreateUpdate): Observable<Empty> {
    return this.cupsHttpService
      .create(cupCreateUpdate)
      .pipe(tap(() => this.reset()));
  }

  public update(
    id: number,
    cupCreateUpdate: CupCreateUpdate
  ): Observable<Empty> {
    return this.cupsHttpService
      .update(id, cupCreateUpdate)
      .pipe(tap(() => this.reset()));
  }

  public delete(id: number): Observable<Empty> {
    return this.cupsHttpService.delete(id).pipe(tap(() => this.reset()));
  }

  setSelectedCup(cupDto: CupDto): void {
    this.selectedCup.next(cupDto);
  }
}
