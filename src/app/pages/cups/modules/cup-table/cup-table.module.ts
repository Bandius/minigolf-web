import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CupTableComponent } from './component/cup-table.component';
import { TableModule } from 'primeng-lts/table';
import { ButtonModule } from 'primeng-lts/button';
import { SharedPipesModule } from '../../../../pipes/shared-pipes.module';
import { CardModule } from 'primeng-lts/card';

@NgModule({
  declarations: [CupTableComponent],
  exports: [CupTableComponent],
  imports: [
    CommonModule,
    TableModule,
    ButtonModule,
    SharedPipesModule,
    CardModule,
  ],
})
export class CupTableModule {}
