import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClubsService } from '../../../../clubs/services/clubs.service';
import { take } from 'rxjs/operators';
import { CupDto } from '../../../../../models/cup-dto';
import { CupsService } from '../../../services/cups.service';
import { ConfirmationService } from '../../../../../services/confirmation.service';

@Component({
  selector: 'app-cups-dialog',
  templateUrl: './cups-dialog.component.html',
  styleUrls: ['./cups-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CupsDialogComponent implements OnInit {
  showDialog: boolean = true;

  formGroup: FormGroup = this.fb.group({
    name: ['', Validators.required],
    text: ['', Validators.required],
    clubsIdPoints: this.fb.array([]),
  });

  @Input() selectedCup: CupDto;

  get clubsIdPointsArray(): FormArray {
    return this.formGroup.get('clubsIdPoints') as FormArray;
  }

  constructor(
    private cupsService: CupsService,
    public clubsService: ClubsService,
    private fb: FormBuilder,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    if (this.selectedCup.id) {
      this.patchFormGroup(this.selectedCup);
    } else {
      this.onPushEmptyClubPoint();
    }
  }

  private patchFormGroup(selectedCup: CupDto): void {
    this.formGroup.patchValue({
      name: selectedCup.name,
      text: selectedCup.text,
    });
    this.clubsIdPointsArray.clear();
    selectedCup.clubsIdPoints.forEach((clubPoint) =>
      this.clubsIdPointsArray.push(
        this.fb.group({
          clubId: [clubPoint.clubId, Validators.required],
          strikes: [clubPoint.strikes, Validators.required],
          rounds: [clubPoint.rounds, Validators.required],
          average: [clubPoint.average, Validators.required],
          points: [clubPoint.points, Validators.required],
        })
      )
    );
  }

  onPushEmptyClubPoint(): void {
    this.clubsIdPointsArray.push(
      this.fb.group({
        clubId: [null, Validators.required],
        strikes: [null, Validators.required],
        rounds: [null, Validators.required],
        average: [null, Validators.required],
        points: [null, Validators.required],
      })
    );
  }

  onRemoveClubPoint(index: number): void {
    this.clubsIdPointsArray.removeAt(index);
  }

  onClose(): void {
    this.cupsService.setSelectedCup(null);
  }

  onSubmit(event: MouseEvent): void {
    if (this.formGroup.valid) {
      if (this.selectedCup.id) {
        this.confirmationService.update(event, () => {
          this.cupsService
            .update(this.selectedCup.id, this.formGroup.value)
            .pipe(take(1))
            .subscribe(() => {
              this.onClose();
            });
        });
      } else {
        this.cupsService
          .create(this.formGroup.value)
          .pipe(take(1))
          .subscribe(() => {
            this.onClose();
          });
      }
    }
  }

  onDelete(event: MouseEvent): void {
    this.confirmationService.delete(event, () => {
      this.cupsService
        .delete(this.selectedCup.id)
        .pipe(take(1))
        .subscribe(() => {
          this.onClose();
        });
    });
  }
}
