import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CupsComponent } from './component/cups.component';

const routes: Routes = [{ path: '', component: CupsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CupsRoutingModule {}
