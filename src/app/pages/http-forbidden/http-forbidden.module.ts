import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpForbiddenComponent } from './component/http-forbidden.component';
import { HttpForbiddenRoutingModule } from './http-forbidden-routing.module';

@NgModule({
  declarations: [HttpForbiddenComponent],
  imports: [CommonModule, HttpForbiddenRoutingModule],
})
export class HttpForbiddenModule {}
