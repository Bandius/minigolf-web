import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../../../services/events.service';
import { ResultCategory } from '../../../models/result-category';
import { Observable } from 'rxjs';
import { EventDto } from '../../../models/event-dto';
import { Score } from '../../../models/score';
import { Routes } from '../../../constants/routes';

@Component({
  selector: 'app-event-write',
  templateUrl: './event-write.component.html',
  styleUrls: ['./event-write.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventWriteComponent implements OnInit {
  eventDto$: Observable<EventDto> = this.eventsService.get(
    Number(this.activatedRoute.snapshot.paramMap.get('id'))
  );

  readonly resultCategory: ResultCategory = ResultCategory.ALL;

  constructor(
    private activatedRoute: ActivatedRoute,
    private eventsService: EventsService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onSelectPlayer(score: Score): void {
    this.router.navigate([Routes.RESULT_WRITE + score.id]);
  }
}
