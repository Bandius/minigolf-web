import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventWriteComponent } from './component/event-write.component';

const routes: Routes = [{ path: ':id', component: EventWriteComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventWriteRoutingModule {}
