import { Pipe, PipeTransform } from '@angular/core';
import { LoginDto } from '../../login/model/login-dto';
import { Role } from '../../login/model/role';

@Pipe({
  name: 'hasPermissions',
})
export class HasPermissionsPipe implements PipeTransform {
  transform(loginDto: LoginDto, eventId: number): boolean {
    return (
      loginDto.role === Role.ADMIN ||
      loginDto.role === Role.OWNER ||
      loginDto.eventId === eventId
    );
  }
}
