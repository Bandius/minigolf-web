import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { switchMap, take } from 'rxjs/operators';
import { EventsService } from '../../../../../services/events.service';
import { RoundCreate } from '../../../../../models/round-create';
import { RoundsService } from '../../../../../services/rounds.service';
import { Calendar } from 'primeng-lts/calendar';
import { EventCreateUpdate } from '../../../../../models/event-create-update';

@Component({
  selector: 'app-events-create-dialog',
  templateUrl: './events-create-dialog.component.html',
  styleUrls: ['./events-create-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsCreateDialogComponent {
  showDialog: boolean = true;

  formGroup: FormGroup = this.fb.group({
    name: ['', Validators.required],
    organizer: ['', Validators.required],
    date: ['', Validators.required],
  });

  @Output() closeEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private eventsService: EventsService,
    private roundsService: RoundsService,
    private fb: FormBuilder
  ) {}

  onClose(updateView?: boolean): void {
    this.closeEvent.emit(updateView);
  }

  onSubmit(): void {
    if (this.formGroup.valid) {
      this.eventsService
        .create(
          new EventCreateUpdate(
            this.formGroup.value.name,
            this.formGroup.value.organizer,
            this.formGroup.value.date[0],
            this.formGroup.value.date[1]
          )
        )
        .pipe(
          switchMap(({ insertId }) =>
            this.roundsService.create(new RoundCreate(insertId, []))
          ),
          take(1)
        )
        .subscribe(() => {
          this.onClose(true);
        });
    }
  }

  onDateSelect(calendar: Calendar): void {
    if (this.formGroup.value.date[0] && this.formGroup.value.date[1]) {
      calendar.toggle();
    }
  }
}
