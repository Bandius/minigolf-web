import { NgModule } from '@angular/core';
import { CoursesComponent } from './component/courses.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{ path: '', component: CoursesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoursesRoutingModule {}
