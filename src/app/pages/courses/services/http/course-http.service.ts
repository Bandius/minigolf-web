import { Injectable } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { Empty } from '../../../../models/empty';
import { CourseDto } from '../../../../models/course-dto';
import { CourseCreateUpdate } from '../../../../models/course-create-update';

@Injectable({
  providedIn: 'root',
})
export class CourseHttpService {
  constructor(private apiService: ApiService) {}

  public getAll(): Observable<CourseDto[]> {
    return this.apiService.get({
      path: '/courses',
      endp: environment.coreEndp,
    });
  }

  public get(id: number): Observable<CourseDto> {
    return this.apiService.get({
      path: '/course/' + id,
      endp: environment.coreEndp,
    });
  }

  public create(courseCreateUpdate: CourseCreateUpdate): Observable<Empty> {
    return this.apiService.post(
      {
        path: '/courses',
        endp: environment.coreEndp,
      },
      courseCreateUpdate
    );
  }

  public update(
    id: number,
    courseCreateUpdate: CourseCreateUpdate
  ): Observable<Empty> {
    return this.apiService.put(
      {
        path: '/course/' + id,
        endp: environment.coreEndp,
      },
      courseCreateUpdate
    );
  }

  public delete(id: number): Observable<Empty> {
    return this.apiService.delete({
      path: '/course/' + id,
      endp: environment.coreEndp,
    });
  }
}
