import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { PlayerCategory } from '../../../../../models/player-category';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PlayerDto } from '../../../../../models/player-dto';
import { PlayersService } from '../../../../players/services/players.service';
import { ClubsService } from '../../../../clubs/services/clubs.service';
import { take } from 'rxjs/operators';
import { CourseDto } from '../../../../../models/course-dto';
import { lanes } from '../../../constants/lanes';
import { CourseService } from '../../../services/course.service';
import { lanesIndex } from '../../../constants/lanesIndex';
import { ConfirmationService } from '../../../../../services/confirmation.service';

@Component({
  selector: 'app-courses-dialog',
  templateUrl: './courses-dialog.component.html',
  styleUrls: ['./courses-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CoursesDialogComponent implements OnInit {
  showDialog: boolean = true;
  lanes: string[] = lanes;
  lanesIndex: number[] = lanesIndex;

  formGroup: FormGroup = this.fb.group({
    name: ['', Validators.required],
    lane1: ['', Validators.required],
    lane2: ['', Validators.required],
    lane3: ['', Validators.required],
    lane4: ['', Validators.required],
    lane5: ['', Validators.required],
    lane6: ['', Validators.required],
    lane7: ['', Validators.required],
    lane8: ['', Validators.required],
    lane9: ['', Validators.required],
    lane10: ['', Validators.required],
    lane11: ['', Validators.required],
    lane12: ['', Validators.required],
    lane13: ['', Validators.required],
    lane14: ['', Validators.required],
    lane15: ['', Validators.required],
    lane16: ['', Validators.required],
    lane17: ['', Validators.required],
    lane18: ['', Validators.required],
  });

  @Input() selectedCourse: CourseDto;

  constructor(
    private courseService: CourseService,
    private fb: FormBuilder,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    if (this.selectedCourse.id) {
      this.patchFormGroup(this.selectedCourse);
    }
  }

  private patchFormGroup(selectedCourse: CourseDto): void {
    this.formGroup.patchValue({
      name: selectedCourse.name,
      lane1: selectedCourse.lane1,
      lane2: selectedCourse.lane2,
      lane3: selectedCourse.lane3,
      lane4: selectedCourse.lane4,
      lane5: selectedCourse.lane5,
      lane6: selectedCourse.lane6,
      lane7: selectedCourse.lane7,
      lane8: selectedCourse.lane8,
      lane9: selectedCourse.lane9,
      lane10: selectedCourse.lane10,
      lane11: selectedCourse.lane11,
      lane12: selectedCourse.lane12,
      lane13: selectedCourse.lane13,
      lane14: selectedCourse.lane14,
      lane15: selectedCourse.lane15,
      lane16: selectedCourse.lane16,
      lane17: selectedCourse.lane17,
      lane18: selectedCourse.lane18,
    });
  }

  onClose(): void {
    this.courseService.setSelectedCourse(null);
  }

  onSubmit(event: MouseEvent): void {
    if (this.formGroup.valid) {
      if (this.selectedCourse.id) {
        this.confirmationService.update(event, () => {
          this.courseService
            .update(this.selectedCourse.id, this.formGroup.value)
            .pipe(take(1))
            .subscribe(() => {
              this.onClose();
            });
        });
      } else {
        this.courseService
          .create(this.formGroup.value)
          .pipe(take(1))
          .subscribe(() => {
            this.onClose();
          });
      }
    }
  }

  onDelete(event: MouseEvent): void {
    this.confirmationService.delete(event, () => {
      this.courseService
        .delete(this.selectedCourse.id)
        .pipe(take(1))
        .subscribe(() => {
          this.onClose();
        });
    });
  }
}
