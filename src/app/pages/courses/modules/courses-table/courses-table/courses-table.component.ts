import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { take } from 'rxjs/operators';
import { CourseService } from '../../../services/course.service';
import { CourseDto } from '../../../../../models/course-dto';
import { lanesIndex } from '../../../constants/lanesIndex';
import { MenuButton } from '../../../../../models/menu-button';
import { MenuAddonsService } from '../../../../../services/menu-addons.service';

@Component({
  selector: 'app-courses-table',
  templateUrl: './courses-table.component.html',
  styleUrls: ['./courses-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CoursesTableComponent implements OnInit, OnDestroy {
  lanes: number[] = lanesIndex;

  constructor(
    public courseService: CourseService,
    private menuAddonsService: MenuAddonsService
  ) {}

  ngOnInit(): void {
    this.menuAddonsService.setButtons([
      new MenuButton(
        'courses-btn',
        this.onSelectCourse.bind(this),
        'Pridať ihrisko',
        'pi pi-plus',
        ''
      ),
    ]);
  }

  onSelectCourse(selectedCourse?: CourseDto): void {
    this.courseService.setSelectedCourse(selectedCourse ?? new CourseDto());
  }

  ngOnDestroy() {
    this.menuAddonsService.setButtons([]);
  }
}
