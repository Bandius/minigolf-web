import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesTableComponent } from './courses-table/courses-table.component';
import { TableModule } from 'primeng-lts/table';
import { ButtonModule } from 'primeng-lts/button';

@NgModule({
  declarations: [CoursesTableComponent],
  exports: [CoursesTableComponent],
  imports: [CommonModule, TableModule, ButtonModule],
})
export class CoursesTableModule {}
