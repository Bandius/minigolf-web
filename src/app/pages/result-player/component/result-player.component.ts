import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ScoresService } from '../../../services/scores.service';
import { Observable } from 'rxjs';
import { Score } from '../../../models/score';
import { Round } from '../../../models/round';
import { switchMap } from 'rxjs/operators';
import { RoundsService } from '../../../services/rounds.service';

@Component({
  selector: 'app-result-player',
  templateUrl: './result-player.component.html',
  styleUrls: ['./result-player.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultPlayerComponent implements OnInit {
  scoreWithRound$: Observable<{ score: Score; rounds: Round[] }> =
    this.scoresService
      .getScore(Number(this.activatedRoute.snapshot.paramMap.get('id')))
      .pipe(
        switchMap(
          (score) => this.roundsService.getRounds(score.eventId).pipe(),
          (score, rounds) => ({ score, rounds })
        )
      );

  constructor(
    private activatedRoute: ActivatedRoute,
    public scoresService: ScoresService,
    private roundsService: RoundsService
  ) {}

  ngOnInit(): void {}
}
