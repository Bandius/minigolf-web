import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultPlayerComponent } from './component/result-player.component';
import { ResultPlayerRoutingModule } from './result-player-routing.module';
import {PlayerTableModule} from "../../shared/player-table/player-table.module";
import {SharedPipesModule} from "../../pipes/shared-pipes.module";
import {PenaltyTableModule} from "../../shared/penalty-table/penalty-table.module";

@NgModule({
  declarations: [ResultPlayerComponent],
  imports: [CommonModule, ResultPlayerRoutingModule, PlayerTableModule, SharedPipesModule, PenaltyTableModule],
})
export class ResultPlayerModule {}
