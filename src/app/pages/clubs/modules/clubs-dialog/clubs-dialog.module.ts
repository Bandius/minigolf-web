import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClubsDialogComponent } from './component/clubs-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DialogModule } from 'primeng-lts/dialog';
import { ButtonModule } from 'primeng-lts/button';
import { InputTextModule } from 'primeng-lts/inputtext';

@NgModule({
  declarations: [ClubsDialogComponent],
  imports: [
    CommonModule,
    DialogModule,
    ButtonModule,
    InputTextModule,
    ReactiveFormsModule,
  ],
  exports: [ClubsDialogComponent],
})
export class ClubsDialogModule {}
