import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClubsService } from '../../../services/clubs.service';
import { take } from 'rxjs/operators';
import { ClubDto } from '../../../../../models/club-dto';
import { ConfirmationService } from '../../../../../services/confirmation.service';

@Component({
  selector: 'app-clubs-dialog',
  templateUrl: './clubs-dialog.component.html',
  styleUrls: ['./clubs-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClubsDialogComponent implements OnInit {
  showDialog: boolean = true;

  formGroup: FormGroup = this.fb.group({
    name: ['', Validators.required],
  });

  @Input() selectedClub: ClubDto;

  constructor(
    private fb: FormBuilder,
    public clubsService: ClubsService,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    if (this.selectedClub.id) {
      this.patchFormGroup(this.selectedClub);
    }
  }

  private patchFormGroup(selectedClub: ClubDto): void {
    this.formGroup.patchValue({
      name: selectedClub.name,
    });
  }

  onClose(): void {
    this.clubsService.setSelectedClub(null);
  }

  onSubmit(event: MouseEvent): void {
    if (this.formGroup.valid) {
      if (this.selectedClub.id) {
        this.confirmationService.update(event, () => {
          this.clubsService
            .update(this.selectedClub.id, this.formGroup.value)
            .pipe(take(1))
            .subscribe(() => {
              this.onClose();
            });
        });
      } else {
        this.clubsService
          .create(this.formGroup.value)
          .pipe(take(1))
          .subscribe(() => {
            this.onClose();
          });
      }
    }
  }

  onDelete(event: MouseEvent): void {
    this.confirmationService.delete(event, () => {
      this.clubsService
        .delete(this.selectedClub.id)
        .pipe(take(1))
        .subscribe(() => {
          this.onClose();
        });
    });
  }
}
