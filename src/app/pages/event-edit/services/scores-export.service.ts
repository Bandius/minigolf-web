import { Injectable } from '@angular/core';
import { ScoreDto } from '../../../models/score-dto';
import { saveAs } from 'file-saver';
import { PlayersService } from '../../players/services/players.service';
import { forkJoin, Observable } from 'rxjs';
import { PlayerDto } from '../../../models/player-dto';
import { ClubDto } from '../../../models/club-dto';
import { ClubsService } from '../../clubs/services/clubs.service';

@Injectable({
  providedIn: 'root',
})
export class ScoresExportService {
  constructor(
    private playersService: PlayersService,
    private clubsService: ClubsService
  ) {}

  public exportXls(scores: ScoreDto[]): void {
    forkJoin({
      players: this.getPlayers(scores),
      clubs: this.getClubs(scores),
    }).subscribe((res) => {
      const table = scores.map((s, i) => {
        const player: PlayerDto = res.players.find((p) => p.id === s.playerId);
        return {
          ['P.č.']: i + 1,
          Meno: player.firstname,
          Priezvisko: player.lastname,
          ['Kat.']: player.category,
          Oddiel: res.clubs.find((c) => c.id === s.clubId).name,
        };
      });
      this.exportExcel(table);
    });
  }

  public exportExcel(table: any): void {
    import('xlsx').then((xlsx) => {
      const worksheet = xlsx.utils.json_to_sheet(table);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: 'xlsx',
        type: 'array',
      });
      this.saveAsExcelFile(excelBuffer, 'turnaj');
    });
  }

  public saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE =
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    saveAs(
      data,
      fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION
    );
  }

  private getPlayers(scores: ScoreDto[]): Observable<PlayerDto[]> {
    return forkJoin(scores.map((s) => this.playersService.get(s.playerId)));
  }

  private getClubs(scores: ScoreDto[]): Observable<ClubDto[]> {
    return forkJoin(scores.map((s) => this.clubsService.get(s.clubId)));
  }
}
