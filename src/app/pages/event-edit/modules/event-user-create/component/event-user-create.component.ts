import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';
import { LoginService } from '../../../../login/services/login.service';
import { UserCreate } from '../../../../login/model/user-create';
import { MessageService } from '../../../../../services/message.service';

@Component({
  selector: 'app-event-user-create',
  templateUrl: './event-user-create.component.html',
  styleUrls: ['./event-user-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventUserCreateComponent {
  showDialog: boolean = true;

  formGroup: FormGroup = this.fb.group({
    login: ['', Validators.required],
    password: ['', Validators.required],
  });

  @Input() eventId: number;
  @Output() closeEvent: EventEmitter<void> = new EventEmitter<void>();

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private messageService: MessageService
  ) {}

  onSubmit(): void {
    this.loginService
      .createUser(
        new UserCreate(
          this.formGroup.value.login,
          this.formGroup.value.password,
          this.eventId
        )
      )
      .pipe(take(1))
      .subscribe(() => {
        this.messageService.success('Účet bol vytvorený.');
        this.onClose();
      });
  }

  onClose(): void {
    this.closeEvent.emit();
  }
}
