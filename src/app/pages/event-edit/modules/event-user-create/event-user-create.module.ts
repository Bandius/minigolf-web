import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventUserCreateComponent } from './component/event-user-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng-lts/inputtext';
import { ButtonModule } from 'primeng-lts/button';
import { DialogModule } from 'primeng-lts/dialog';

@NgModule({
  declarations: [EventUserCreateComponent],
  imports: [
    CommonModule,
    DialogModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
  ],
  exports: [EventUserCreateComponent],
})
export class EventUserCreateModule {}
