import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventDto } from '../../../models/event-dto';
import { EventsService } from '../../../services/events.service';
import { MessageService } from '../../../services/message.service';
import { ConfirmationService } from '../../../services/confirmation.service';
import { shareReplay, take } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Routes } from '../../../constants/routes';
import { Observable } from 'rxjs';
import { EventCreateUpdate } from '../../../models/event-create-update';
import { PlayersService } from '../../players/services/players.service';
import { ClubsService } from '../../clubs/services/clubs.service';
import { MenuButton } from '../../../models/menu-button';
import { MenuAddonsService } from '../../../services/menu-addons.service';
import { EventScoresComponent } from '../modules/event-scores/component/event-scores.component';
import { SnapshotService } from '../services/snapshot.service';
import { SnapshotCreate } from '../../../models/snapshot-create';
import { ScoresExportService } from '../services/scores-export.service';
import { Calendar } from 'primeng-lts/calendar';
import { EventStateUpdate } from '../../../models/event-state-update';
import { EventState, getNextEventState } from '../../../models/event-state';
import { DatePipe } from '@angular/common';
import { LoginService } from '../../login/services/login.service';
import { Role } from '../../login/model/role';

@Component({
  selector: 'app-event-edit',
  templateUrl: './event-edit.component.html',
  styleUrls: ['./event-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventEditComponent implements OnInit, OnDestroy {
  eventDto$: Observable<EventDto> = this.eventsService
    .get(Number(this.activatedRoute.snapshot.paramMap.get('id')))
    .pipe(shareReplay());

  formGroup: FormGroup = this.fb.group({
    id: [null, Validators.required],
    name: ['', Validators.required],
    organizer: ['', Validators.required],
    date: ['', Validators.required],
  });

  showUserCreateDialog: boolean;
  isAdmin: boolean =
    this.loginService.loginValue.role === Role.ADMIN ||
    this.loginService.loginValue.role === Role.OWNER;

  @ViewChild(EventScoresComponent) eventScoresComponent: EventScoresComponent;

  constructor(
    private activatedRoute: ActivatedRoute,
    private eventsService: EventsService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private changeDetectorRef: ChangeDetectorRef,
    private confirmationService: ConfirmationService,
    private router: Router,
    public playersService: PlayersService,
    public clubsService: ClubsService,
    private menuAddonsService: MenuAddonsService,
    private snapshotService: SnapshotService,
    private scoresExportService: ScoresExportService,
    private datePipe: DatePipe,
    private loginService: LoginService
  ) {}

  ngOnInit(): void {
    this.eventDto$.subscribe((eventDto) => {
      this.patchFormGroup(eventDto);
      this.setMenuButtons(eventDto);
      this.changeDetectorRef.detectChanges();
    });
  }

  private setMenuButtons(eventDto: EventDto): void {
    const buttons0 = [
      new MenuButton(
        'event-edit-btn',
        this.onExportXls.bind(this),
        'Stiahnuť XLS',
        'pi pi-download',
        'p-button-success',
        true
      ),
      new MenuButton(
        'event-edit-btn',
        this.onTakeSnapshot.bind(this),
        'Uložiť poradie',
        'pi pi-save',
        'p-button-success',
        true
      ),
    ];

    if (this.isAdmin) {
      this.menuAddonsService.setButtons([
        new MenuButton(
          'event-user-create-btn',
          this.onShowUserCreateDialog.bind(this, true),
          'Pridať zapisovateľa',
          'pi pi-plus',
          'p-button-info',
          true
        ),
        new MenuButton(
          'event-active-btn',
          this.onSetActive.bind(this, eventDto),
          eventDto.state === EventState.AVAILABLE ? 'Zamknúť' : 'Odomknúť',
          'pi ' +
            (eventDto.state === EventState.AVAILABLE
              ? 'pi-lock'
              : 'pi-lock-open'),
          'p-button-info',
          true
        ),
        ...buttons0,
        new MenuButton(
          'event-edit-btn',
          this.onDelete.bind(this),
          'Zmazať turnaj',
          'pi pi-trash',
          'p-button-danger',
          true
        ),
      ]);
    } else {
      this.menuAddonsService.setButtons(buttons0);
    }
  }

  private patchFormGroup(eventDto: EventDto): void {
    this.formGroup.patchValue({
      id: eventDto.id,
      name: eventDto.name,
      organizer: eventDto.organizer,
      date: [eventDto.dateFrom, eventDto.dateTo],
    });
  }

  private getDate(date: string): string {
    return !date?.length ? this.datePipe.transform(date, 'dd.MM.yyyy') : date;
  }

  onDateSelect(calendar: Calendar): void {
    if (this.formGroup.value.date[0] && this.formGroup.value.date[1]) {
      calendar.toggle();
    }
  }

  onDelete(event: MouseEvent): void {
    this.confirmationService.delete(event, () => {
      this.eventsService
        .delete(this.formGroup.value.id)
        .pipe(take(1))
        .subscribe(() => {
          this.messageService.success('Turnaj bol vymazaný.');
          this.router.navigate([Routes.EVENTS]);
        });
    });
  }

  onEventSubmit(): void {
    if (this.formGroup.valid) {
      this.eventsService
        .update(
          this.formGroup.value.id,
          new EventCreateUpdate(
            this.formGroup.value.name,
            this.formGroup.value.organizer,
            this.getDate(this.formGroup.value.date[0]),
            this.getDate(this.formGroup.value.date[1])
          )
        )
        .pipe(take(1))
        .subscribe(() => {
          this.messageService.success('Turnaj bol upravený.');
        });
    }
  }

  onSetActive(eventDto: EventDto): void {
    const newState: EventState = getNextEventState(eventDto.state);
    this.eventsService
      .updateState(eventDto.id, new EventStateUpdate(newState))
      .subscribe(() => {
        eventDto.state = newState;
        this.messageService.success('Stav turnaja bol zmenený');
        this.setMenuButtons(eventDto);
      });
  }

  onTakeSnapshot(): void {
    const ids = this.eventScoresComponent.scores
      .map((s) => s.id)
      .filter((v) => v);
    this.snapshotService
      .create(new SnapshotCreate(this.formGroup.value.id, JSON.stringify(ids)))
      .subscribe(() => {
        this.messageService.success('Poradie bolo uložené.');
      });
  }

  onExportXls(): void {
    this.scoresExportService.exportXls(
      this.eventScoresComponent.scores.filter((s) => s.id)
    );
  }

  onShowUserCreateDialog(show: boolean): void {
    this.showUserCreateDialog = show;
    this.changeDetectorRef.detectChanges();
  }

  ngOnDestroy() {
    this.menuAddonsService.setButtons([]);
  }
}
