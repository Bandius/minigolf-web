import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayersTableComponent } from './component/players-table.component';
import { TableModule } from 'primeng-lts/table';
import {ButtonModule} from "primeng-lts/button";
import {SharedPipesModule} from "../../../../pipes/shared-pipes.module";

@NgModule({
  declarations: [PlayersTableComponent],
  imports: [CommonModule, TableModule, ButtonModule, SharedPipesModule],
  exports: [
    PlayersTableComponent
  ]
})
export class PlayersTableModule {}
