import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import { PlayersService } from '../../../services/players.service';
import { PlayerDto } from '../../../../../models/player-dto';
import { MenuAddonsService } from '../../../../../services/menu-addons.service';
import { MenuButton } from '../../../../../models/menu-button';

@Component({
  selector: 'app-players-table',
  templateUrl: './players-table.component.html',
  styleUrls: ['./players-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlayersTableComponent implements OnInit, OnDestroy {
  constructor(
    public playersService: PlayersService,
    private menuAddonsService: MenuAddonsService
  ) {}

  ngOnInit(): void {
    this.menuAddonsService.setButtons([
      new MenuButton(
        'players-table-btn',
        this.onSelectPlayer.bind(this),
        'Pridať hráča',
        'pi pi-plus',
        ''
      ),
    ]);
  }

  onSelectPlayer(selectedPlayer?: PlayerDto): void {
    this.playersService.setSelectedPlayer(selectedPlayer ?? new PlayerDto());
  }

  ngOnDestroy() {
    this.menuAddonsService.setButtons([]);
  }
}
