import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerDialogComponent } from './component/player-dialog.component';
import { DialogModule } from 'primeng-lts/dialog';
import { DropdownModule } from 'primeng-lts/dropdown';
import { ReactiveFormsModule } from '@angular/forms';
import {ButtonModule} from "primeng-lts/button";
import {MultiSelectModule} from "primeng-lts/multiselect";
import {InputTextModule} from "primeng-lts/inputtext";

@NgModule({
  declarations: [PlayerDialogComponent],
  exports: [PlayerDialogComponent],
  imports: [CommonModule, DialogModule, ButtonModule, InputTextModule, MultiSelectModule, DropdownModule, ReactiveFormsModule],
})
export class PlayerDialogModule {}
