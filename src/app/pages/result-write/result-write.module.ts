import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultWriteComponent } from './component/result-write.component';
import { ResultWriteRoutingModule } from './result-write-routing.module';
import { PlayerTableModule } from '../../shared/player-table/player-table.module';
import { SharedPipesModule } from '../../pipes/shared-pipes.module';
import { InputNumberModule } from 'primeng-lts/inputnumber';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng-lts/button';
import { PenaltyTableModule } from '../../shared/penalty-table/penalty-table.module';

@NgModule({
  declarations: [ResultWriteComponent],
  imports: [
    CommonModule,
    ResultWriteRoutingModule,
    PlayerTableModule,
    SharedPipesModule,
    InputNumberModule,
    ReactiveFormsModule,
    ButtonModule,
    PenaltyTableModule,
  ],
})
export class ResultWriteModule {}
