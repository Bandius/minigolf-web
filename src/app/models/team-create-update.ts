import { TeamDto } from './team-dto';

export class TeamCreateUpdate {
  eventId: number;
  clubId: number;
  player1: number;
  player2: number;
  player3: number;
  player4: number;
  player5: number;
  player6: number;

  constructor(
    eventId: number,
    clubId: number,
    player1: number,
    player2: number,
    player3: number,
    player4: number,
    player5: number,
    player6: number
  ) {
    this.eventId = eventId;
    this.clubId = clubId;
    this.player1 = player1;
    this.player2 = player2;
    this.player3 = player3;
    this.player4 = player4;
    this.player5 = player5;
    this.player6 = player6;
  }

  static create(
    eventId: number,
    clubId: number,
    playerId: number,
    nasadenie: number
  ): TeamCreateUpdate {
    return new TeamCreateUpdate(
      eventId,
      clubId,
      nasadenie === 1 ? playerId : null,
      nasadenie === 2 ? playerId : null,
      nasadenie === 3 ? playerId : null,
      nasadenie === 4 ? playerId : null,
      nasadenie === 5 ? playerId : null,
      nasadenie === 6 ? playerId : null
    );
  }

  static update(
    teamDto: TeamDto,
    playerId: number,
    nasadenie: number
  ): TeamCreateUpdate {
    return new TeamCreateUpdate(
      teamDto.eventId,
      teamDto.clubId,
      nasadenie === 1 ? playerId : teamDto.player1,
      nasadenie === 2 ? playerId : teamDto.player2,
      nasadenie === 3 ? playerId : teamDto.player3,
      nasadenie === 4 ? playerId : teamDto.player4,
      nasadenie === 5 ? playerId : teamDto.player5,
      nasadenie === 6 ? playerId : teamDto.player6
    );
  }

  static delete(teamDto: TeamDto, playerId: number): TeamCreateUpdate {
    return new TeamCreateUpdate(
      teamDto.eventId,
      teamDto.clubId,
      teamDto.player1 === playerId ? null : teamDto.player1,
      teamDto.player2 === playerId ? null : teamDto.player2,
      teamDto.player3 === playerId ? null : teamDto.player3,
      teamDto.player4 === playerId ? null : teamDto.player4,
      teamDto.player5 === playerId ? null : teamDto.player5,
      teamDto.player6 === playerId ? null : teamDto.player6
    );
  }
}
