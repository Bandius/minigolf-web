export class ScoreRound {
  score: number[];
  sum: number;
  penalty: number;

  constructor(score: number[], sum: number, penalty: number) {
    this.score = score;
    this.sum = sum;
    this.penalty = penalty ?? 0;
  }

  static fromScore(score: number[], penalty: number): ScoreRound {
    return new ScoreRound(score, ScoreRound.roundSum(score), penalty);
  }

  static roundSum(score: number[]): number {
    return score.reduce((sum, v) => sum + v ?? 0, 0);
  }

  static fullRound(): ScoreRound {
    return new ScoreRound([], 18 * 7, 0);
  }
}
