import {ClubPoint} from "./club-point";

export interface CupCreateUpdate {
  name: string;
  text: string;
  clubsIdPoints: ClubPoint[];
}
