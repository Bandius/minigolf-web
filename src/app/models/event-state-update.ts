import { EventState } from './event-state';

export class EventStateUpdate {
  state: EventState;

  constructor(state: EventState) {
    this.state = state;
  }
}
