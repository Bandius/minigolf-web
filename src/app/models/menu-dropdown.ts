export class MenuDropdown {
  action: (...args) => void;
  options: any[];

  constructor(action: (...args) => void, options: any[]) {
    this.action = action;
    this.options = options;
  }
}
