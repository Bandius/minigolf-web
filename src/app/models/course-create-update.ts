export interface CourseCreateUpdate {
  name: string;
  lane1: string;
  lane2: string;
  lane3: string;
  lane4: string;
  lane5: string;
  lane6: string;
  lane7: string;
  lane8: string;
  lane9: string;
  lane10: string;
  lane11: string;
  lane12: string;
  lane13: string;
  lane14: string;
  lane15: string;
  lane16: string;
  lane17: string;
  lane18: string;
}
