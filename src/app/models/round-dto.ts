import { Round } from './round';

export class RoundDto {
  id: number;
  created: string;
  eventId: number;
  rounds: Round[];
}
