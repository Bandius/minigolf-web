export class ScoreUpdate {
  score: number[][];

  constructor(score: number[][]) {
    this.score = score;
  }
}
