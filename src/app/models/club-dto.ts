export class ClubDto {
  id: number;
  created: string;
  name: string;
}
