import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Empty } from '../models/empty';
import { RoundDto } from '../models/round-dto';
import { RoundsHttpService } from './http/rounds-http.service';
import { RoundCreate } from '../models/round-create';
import { RoundUpdate } from '../models/round-update';
import { Round } from '../models/round';

@Injectable({
  providedIn: 'root',
})
export class RoundsService {
  constructor(private roundsHttpService: RoundsHttpService) {}

  public get(eventId: number): Observable<RoundDto> {
    return this.roundsHttpService.get(eventId);
  }

  public getRounds(eventId: number): Observable<Round[]> {
    return this.roundsHttpService
      .get(eventId)
      .pipe(map((roundDto) => roundDto?.rounds));
  }

  public create(roundCreate: RoundCreate): Observable<Empty> {
    return this.roundsHttpService.create(roundCreate);
  }

  public update(id: number, roundUpdate: RoundUpdate): Observable<Empty> {
    return this.roundsHttpService.update(id, roundUpdate);
  }
}
