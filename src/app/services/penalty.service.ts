import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Empty } from '../models/empty';
import { PenaltyDto } from '../models/penalty-dto';
import { PenaltyHttpService } from './http/penalty-http.service';
import { PenaltyCreateUpdate } from '../models/penalty-create-update';
import { EventsService } from './events.service';

@Injectable({
  providedIn: 'root',
})
export class PenaltyService {
  constructor(
    private eventsService: EventsService,
    private penaltyHttpService: PenaltyHttpService
  ) {}

  public getAll(eventId: number): Observable<PenaltyDto[]> {
    return this.penaltyHttpService.getAll(eventId);
  }

  public get(scoreId: number): Observable<PenaltyDto[]> {
    return this.penaltyHttpService.get(scoreId);
  }

  public create(penaltyCreateUpdate: PenaltyCreateUpdate): Observable<Empty> {
    return this.penaltyHttpService.create(penaltyCreateUpdate);
  }

  public update(
    id: number,
    penaltyCreateUpdate: PenaltyCreateUpdate
  ): Observable<Empty> {
    return this.penaltyHttpService.update(id, penaltyCreateUpdate);
  }

  public delete(id: number): Observable<Empty> {
    return this.penaltyHttpService.delete(id);
  }
}
