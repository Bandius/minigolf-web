import { Injectable } from '@angular/core';
import { MessageService as MessageServicePrimeNG } from 'primeng-lts/api';

interface Timeouts {
  [key: string]: number;
}

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  static SUCCESS = 3000;
  static INFO = 3000;
  static WARN = 10000;
  static ERROR = 15000;

  private timeouts: Timeouts = {};

  constructor(private messageService: MessageServicePrimeNG) {}

  public success(text: string, data?: any, sticky?: boolean, key?: string): void {
    this.messageService.add({
      severity: 'success',
      summary: 'Úspech',
      detail: text,
      life: MessageService.SUCCESS,
      closable: true,
      data,
      sticky,
      key,
    });
  }

  public info(text: string, data?: any, sticky?: boolean, key?: string): void {
    this.messageService.add({
      severity: 'info',
      summary: 'Informácia',
      detail: text,
      life: MessageService.INFO,
      closable: true,
      data,
      sticky,
      key,
    });
  }

  public warn(text: string, data?: any, sticky?: boolean, key?: string): void {
    this.messageService.add({
      severity: 'warn',
      summary: new Date().toLocaleString(),
      detail: text,
      life: MessageService.WARN,
      closable: true,
      data,
      sticky,
      key,
    });
  }

  public error(text: string, data?: any, sticky?: boolean, key?: string): void {
    this.messageService.add({
      severity: 'error',
      summary: new Date().toLocaleString(),
      detail: text,
      life: MessageService.ERROR,
      closable: true,
      data,
      sticky,
      key,
    });
  }

  public clear(key?: string): void {
    this.messageService.clear(key);
  }

  public setLife(key: string, life: number): void {
    clearTimeout(this.timeouts[key]);
    // this.timeouts[key] = setTimeout(() => {
    //   this.messageService.clear(key);
    // }, life);
  }

}
