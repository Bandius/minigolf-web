import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Empty } from '../../models/empty';
import { PenaltyDto } from '../../models/penalty-dto';
import { PenaltyCreateUpdate } from '../../models/penalty-create-update';

@Injectable({
  providedIn: 'root',
})
export class PenaltyHttpService {
  constructor(private apiService: ApiService) {}

  public getAll(eventId: number): Observable<PenaltyDto[]> {
    return this.apiService.get({
      path: '/penalty/' + eventId,
      endp: environment.coreEndp,
    });
  }

  public get(scoreId: number): Observable<PenaltyDto[]> {
    return this.apiService.get({
      path: '/penalty/' + scoreId + '/score',
      endp: environment.coreEndp,
    });
  }

  public create(penaltyCreateUpdate: PenaltyCreateUpdate): Observable<Empty> {
    return this.apiService.post(
      {
        path: '/penalty',
        endp: environment.coreEndp,
      },
      penaltyCreateUpdate
    );
  }

  public update(
    id: number,
    penaltyCreateUpdate: PenaltyCreateUpdate
  ): Observable<Empty> {
    return this.apiService.put(
      {
        path: '/penalty/' + id,
        endp: environment.coreEndp,
      },
      penaltyCreateUpdate
    );
  }

  public delete(id: number): Observable<Empty> {
    return this.apiService.delete({
      path: '/penalty/' + id,
      endp: environment.coreEndp,
    });
  }
}
