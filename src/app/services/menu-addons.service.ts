import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { MenuButton } from '../models/menu-button';
import { MenuDropdown } from '../models/menu-dropdown';

@Injectable({
  providedIn: 'root',
})
export class MenuAddonsService {
  private menuButtons: BehaviorSubject<MenuButton[]> = new BehaviorSubject<
    MenuButton[]
  >([]);
  private menuDropdown: BehaviorSubject<MenuDropdown> =
    new BehaviorSubject<MenuDropdown>(null);

  constructor() {}

  get menuButtons$(): Observable<MenuButton[]> {
    return this.menuButtons.asObservable();
  }

  get menuDropdown$(): Observable<MenuDropdown> {
    return this.menuDropdown.asObservable();
  }

  setButtons(menuButtons: MenuButton[]): void {
    setTimeout(() => {
      this.menuButtons.next(menuButtons);
    }, 0);
  }

  setMenuDropdown(menuDropdown: MenuDropdown): void {
    setTimeout(() => {
      this.menuDropdown.next(menuDropdown);
    }, 0);
  }
}
