import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng-lts/api';
import { LoginService } from './pages/login/services/login.service';
import { MenuAddonsService } from './services/menu-addons.service';
import { PresentationService } from './services/presentation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  constructor(
    public loginService: LoginService,
    public menuAddonsService: MenuAddonsService,
    public presentationService: PresentationService
  ) {}

  items: MenuItem[] = [
    { label: 'Domov', routerLink: ['/home'], icon: 'pi pi-fw pi-home' },
    {
      label: 'Turnaje',
      routerLink: ['/results'],
      icon: 'pi pi-fw pi-calendar',
    },
    {
      label: 'Databáza',
      icon: 'pi pi-fw pi-folder',
      state: { secureRead: true },
      items: [
        { label: 'Hráči', routerLink: ['/players'], icon: 'pi pi-fw pi-users' },
        {
          label: 'Ihriská',
          routerLink: ['/courses'],
          icon: 'pi pi-fw pi-map',
        },
        { label: 'Kluby', routerLink: ['/clubs'], icon: 'pi pi-fw pi-home' },
        { label: 'Ligy', routerLink: ['/cups'], icon: 'pi pi-fw pi-star-o' },
        {
          label: 'Turnaje',
          routerLink: ['/events'],
          icon: 'pi pi-fw pi-sun',
        },
      ],
    },
    {
      label: 'Prihlásenie',
      routerLink: ['/login'],
      icon: 'pi pi-fw pi-user',
    },
  ];

  ngOnInit() {
    this.loginService.tryLogin();
  }
}
